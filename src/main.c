#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "wav.h"
#include "Logger.h"

_Bool silent = 0;
_Bool overwrite = 0;


int main(int argc, char** argv)
{
	char file_name[FILENAME_MAX];
	wav_file_t* file;
	uint32_t curr_duration;
	uint32_t new_duration;
	int ch;

	while ((ch = getopt(argc, argv, "so")) != -1)
	{
		switch (ch)
		{
			case 's': silent    = 1; break;
			case 'o': overwrite = 1; break;
		}
	}

	INFO("File path: ");
	fgets(file_name, FILENAME_MAX - 1, stdin);
	file_name[strcspn(file_name, "\r\n")] = '\0';

	if (!match_file_extension(file_name, ".wav"))
	{
		ERROR("Unsupported file extension");
	}

	file = load_wav(file_name);

	if (!is_wav_valid(file))
	{
		ERROR("Invalid .wav file");
	}

	print_wav_info(file);

	curr_duration = get_wav_duration(file);

	INFO("The length of the file content is %02u:%02u:%02u\n",
										curr_duration / 3600,
									   	(curr_duration % 3600) / 60,
									   	curr_duration % 60);

	new_duration = input_duration();

	if (new_duration > curr_duration)
	{
		extend_wav(&file, new_duration);
	}
	else if (new_duration < curr_duration)
	{
		trim_wav(file, new_duration);
	}
	else
	{
		free(file);
		return 0;
	}

	save_wav(file, file_name);

	free(file);
	return 0;
}
