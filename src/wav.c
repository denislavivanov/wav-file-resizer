#include "wav.h"
#include "Logger.h"
#include <stdlib.h>
#include <string.h>

extern _Bool silent;
extern _Bool overwrite;

wav_file_t* load_wav(const char* path)
{
	FILE*    fp;
	void*    buffer;
	size_t   file_size;

	fp = fopen(path, "rb");

	if (fp == NULL)
	{
		ERROR("Can't open file: check the file path");
	}

	fseek(fp, 0, SEEK_END);
	file_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	buffer = malloc(file_size);

	if (buffer == NULL)
	{
		ERROR("malloc failed - Possible reason: Insufficient memory");
	}

	fread(buffer, file_size, 1, fp);
	fclose(fp);

	return buffer;
}

void save_wav(wav_file_t* file, const char* path)
{
	FILE* fp;
	char modified_path[FILENAME_MAX];
	const char* suffix = "-modified.wav";
	size_t suffix_len = strlen(suffix);
	size_t path_len   = strlen(path);

	strncpy(modified_path, path, FILENAME_MAX - suffix_len - 1);
	strncpy(modified_path + path_len - 4, suffix, suffix_len + 1);

	if (overwrite)
	{
		if (rename(path, modified_path) != 0)
		{
			ERROR("rename failed");
		}
	}

	fp = fopen(modified_path, "wb");

	fwrite(file, (size_t)file->ChunkSize + 8, 1, fp);
	fclose(fp);
}

_Bool is_wav_valid(wav_file_t* file)
{
	if (file->ChunkID != 0x46464952)
		return 0;

	if (file->Format != 0x45564157)
		return 0;

	if (file->Subchunk1ID != 0x20746d66)
		return 0;

	if (file->Subchunk2ID != 0x61746164)
		return 0;

	return 1;
}

uint32_t input_duration(void)
{
	char buffer[10] = { 0 };
	uint32_t hh, mm, ss;

	INFO("\nNew duration: ");
	scanf("%8s", buffer);

	if (strlen(buffer) != 8
		|| sscanf(buffer, "%2u:%2u:%2u", &hh, &mm, &ss) != 3)
	{
		ERROR("Invalid duration format - should be <hh>:<mm>:<ss>\n");
	}

	if (mm > 59)
	{
		ERROR("Invalid duration format - <mm> should be in the range 0-59\n");
	}

	if (ss > 59)
	{
		ERROR("Invalid duration format - <ss> should be in the range 0-59\n");
	}

	return hh * 3600 + mm * 60 + ss;
}

_Bool match_file_extension(const char* path, const char* extension)
{
	size_t plen = strlen(path);
	size_t elen = strlen(extension);

	if (elen > plen)
		return 0;

	return strcmp(&path[plen-elen], extension) == 0;
}

void extend_wav(wav_file_t** file, uint32_t new_duration)
{
	wav_file_t* temp = *file;
	uint32_t new_size = new_duration *
						(temp->SampleRate * temp->ChannelNum * temp->BitsPerSample / 8);

	temp = realloc(temp, (size_t)new_size + 44);

	if (temp == NULL)
	{
		ERROR("realloc failed: Possible reason - insufficient memory");
	}

	memset(&temp->data + temp->Subchunk2Size, 0, new_size - temp->Subchunk2Size);

	temp->ChunkSize = 36 + new_size;
	temp->Subchunk2Size = new_size;

	*file = temp;
}

void print_wav_info(wav_file_t* file)
{
	INFO("\n");
	INFO("Channel num: %hu\n", file->ChannelNum);
	INFO("Sample Rate: %u\n", file->SampleRate);
	INFO("Byte Rate: %u\n", file->ByteRate);
	INFO("Bits per sample: %hu\n", file->BitsPerSample);
	INFO("\n");
}

void trim_wav(wav_file_t* file, uint32_t new_duration)
{
	uint32_t new_size = new_duration *
		(file->SampleRate * file->ChannelNum * file->BitsPerSample / 8);

	file->ChunkSize = 36 + new_size;
	file->Subchunk2Size = new_size;
}

uint32_t get_wav_duration(wav_file_t* file)
{
	return file->Subchunk2Size /
			(file->SampleRate * file->ChannelNum * file->BitsPerSample / 8);
}
