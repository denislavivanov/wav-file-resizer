#ifndef WAV_H
#define WAV_H

#include <stdint.h>

typedef struct wav_file_t
{
	/* RIFF descriptor */
	uint32_t ChunkID;
	uint32_t ChunkSize;
	uint32_t Format;

	/* fmt sub-chunk */
	uint32_t Subchunk1ID;
	uint32_t Subchunk1Size;
	uint16_t AudioFormat;
	uint16_t ChannelNum;
	uint32_t SampleRate;
	uint32_t ByteRate;
	uint16_t BlockAlign;
	uint16_t BitsPerSample;

	/* data sub-chunk */
	uint32_t Subchunk2ID;
	uint32_t Subchunk2Size;
	uint8_t  data;
} wav_file_t;

/* Loads .wav file from disk */
wav_file_t* load_wav(const char* path);

/* Checks the validity of the .wav file */
_Bool is_wav_valid(wav_file_t* file);

/* Checks file's extension */
_Bool match_file_extension(const char* path, const char* extension);

/* Pads .wav file with silence */
void extend_wav(wav_file_t** file, uint32_t new_duration);

/* Trims the .wav file */
void trim_wav(wav_file_t* file, uint32_t new_duration);

/* Saves a .wav file to disk */
void save_wav(wav_file_t* file, const char* path);

/* Prints meta data information about .wav file */
void print_wav_info(wav_file_t* file);

/* Returns duration in seconds of .wav file */
uint32_t get_wav_duration(wav_file_t* file);

/* Receives new duration from stdin */
uint32_t input_duration(void);

#endif
