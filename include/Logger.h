#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>

#define ERROR(msg) \
		if (!silent) { \
			printf("\n%s at line %d: [ERROR]: %s\n", __func__, __LINE__, msg); \
		} \
		exit(1);

#define INFO(...) \
		if (!silent) { \
			printf(__VA_ARGS__); \
		}
#endif
